export const ID_URL = process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? 'https://tk-id-app.herokuapp.com/' : 'https://id.techkids.vn/';
export const LOGOUT_URL = `${ID_URL}/?logout=true`;

export const API_URL = process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? 'https://tk-lms-dev.herokuapp.com/api/v1' : 'https://lm.techkids.vn/api/v1';
export const API_TEACHER_URL = `${API_URL}/teacher`;
export const API_AUTH_URL = `${API_URL}/auth`;
