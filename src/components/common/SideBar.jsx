import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  // Link,
  Divider,
  Drawer,
  Hidden,
  List,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Icon,
} from '@material-ui/core';
import classNames from 'classnames';

export default class SideBar extends PureComponent {
  render() {
    const {
      classes, theme, mobileOpen, open, handleDrawerClose,
    } = this.props;

    const drawer = (
      <div>
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <Icon>chevron_right</Icon>
            ) : (
              <Icon>chevron_left</Icon>
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          {[{ label: 'Student checkin', link: '/', icon: 'playlist_add_check' }].map(
            ({ label, link, icon }) => (
              <Link to={link} key={label} onClick={handleDrawerClose}>
                <ListItem button>
                  <ListItemIcon>
                    <Icon>{icon}</Icon>
                  </ListItemIcon>
                  <ListItemText primary={label} />
                </ListItem>
              </Link>
            ),
          )}
        </List>
      </div>
    );

    return (
      <nav className={classes.drawer}>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            // container={this.props.container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerClose}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            variant="permanent"
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            })}
            classes={{
              paper: classNames({
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
              }),
            }}
            open={open}
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    );
  }
}

SideBar.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  theme: PropTypes.shape({}).isRequired,
  mobileOpen: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
  handleDrawerClose: PropTypes.func.isRequired,
};
