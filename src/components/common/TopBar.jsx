/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  CircularProgress,
} from '@material-ui/core';
import { Menu as MenuIcon } from '@material-ui/icons';

export default class TopBar extends Component {
  render() {
    const {
      classes, open, handleDrawerOpen, topLoading,
    } = this.props;
    let showLoading = false;

    if (topLoading) {
      for (let i = 0; i < Object.keys(topLoading).length; i += 1) {
        const key = Object.keys(topLoading)[i];
        if (topLoading[key] === true) {
          showLoading = true;
        }
      }
    }

    return (
      <AppBar
        position="fixed"
        className={classNames(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar disableGutters={!open}>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            className={classNames(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.grow} variant="h6" color="inherit" noWrap>
            Student checkin & classnote
          </Typography>
          { showLoading
            ? <CircularProgress className={classes.progress} size={30} thickness={5} />
            : ''}
        </Toolbar>
      </AppBar>
    );
  }
}

TopBar.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  topLoading: PropTypes.shape({}).isRequired,
  open: PropTypes.bool.isRequired,
  handleDrawerOpen: PropTypes.func.isRequired,
};
