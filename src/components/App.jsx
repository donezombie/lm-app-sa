import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Switch, Route, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

import Attendance from './StudentAttendance/List';
import SideBar from './common/SideBar';
import TopBar from './common/TopBar';

import Login from './Login';

import { ID_URL, API_AUTH_URL } from '../urls';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  grow: {
    flexGrow: 1,
  },
  progress: {
    margin: theme.spacing.unit * 2,
    color: '#ffffff',
  },
});

const loadingContainerStyle = {
  height: '100vh',
  display: 'flex',
  justifyContent: 'center',
  justifyItems: 'center',
};

class App extends Component {
  constructor() {
    super();

    this.state = {
      mobileOpen: false,
      open: false,
      topLoading: {},
    };

    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
    this.pushQueueTopLoading = this.pushQueueTopLoading.bind(this);
  }

  handleDrawerOpen() {
    this.setState({ open: true, mobileOpen: true });
  }

  handleDrawerClose() {
    this.setState({ open: false, mobileOpen: false });
  }

  pushQueueTopLoading(_id) {
    const { topLoading } = this.state;
    if (topLoading[_id] !== undefined) {
      topLoading[_id] = !topLoading[_id];
    } else {
      topLoading[_id] = true;
    }
    this.setState({ topLoading });
  }

  render() {
    const { classes } = this.props;
    const {
      mobileOpen, open, topLoading,
    } = this.state;
    return (

      <Login
        API_AUTH_URL={API_AUTH_URL}
        ID_URL={ID_URL}
      >
        <div className={classes.root}>
          <CssBaseline />
          <TopBar
            {...this.props}
            open={open}
            handleDrawerOpen={this.handleDrawerOpen}
            topLoading={topLoading}
          />
          <SideBar
            {...this.props}
            open={open}
            mobileOpen={mobileOpen}
            handleDrawerClose={this.handleDrawerClose}
          />
          <main
            className={classNames(classes.content)}
          >
            <Switch>
              <Route
                exact
                path="/"
                render={() => (
                  <Attendance
                    pushQueueTopLoading={this.pushQueueTopLoading}
                  />
                )}
              />
            </Switch>
          </main>
        </div>
      </Login>
    );
  }
}

App.propTypes = {
  classes: PropTypes.shape({}).isRequired,
};

export default withStyles(styles, { withTheme: true })(withRouter(App));
