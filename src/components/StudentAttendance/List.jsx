import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid, FormControl, Select, MenuItem,
} from '@material-ui/core';
import moment from 'moment';

import {
  fetchCourses, fetchClassById, fetchClassesInCourse, updateClass,
} from '../../networks';
import AttendanceTable from './AttendanceTable';
import loadingIcon from '../loading.svg';

moment.locale('vi');

const styles = theme => ({
  root: {
    flexGrow: 1,
    paddingTop: 70,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  formControl: {
    width: '90%',
  },
  loadingContainer: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    justifyItems: 'center',
    paddingTop: 70,
  },
});

class Attendance extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courses: [],
      currentCourseClassrooms: [],
      currentClassroom: '',
      currentCourse: '',
      attendances: null,
      date: moment().format('DD-MM-YYYY'),
      loading: true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.studentAttended = this.studentAttended.bind(this);
  }

  async componentDidMount() {
    const { date } = this.state;

    const courses = await fetchCourses();
    const currentCourse = courses[0] && courses[0]._id ? courses[0] : '';
    const currentCourseClassrooms = currentCourse
      ? await fetchClassesInCourse(currentCourse._id) : [];
    const currentClassroom = currentCourseClassrooms[0]
      ? await fetchClassById(currentCourseClassrooms['0']._id) : '';
    let attendances = null;

    if (_.get(currentClassroom, 'schedule.items.length')) {
      attendances = currentClassroom.schedule.items
        .filter(item => moment(item.start).format('DD-MM-YYYY') === date)
        .map(item => item.attendance)[0] || null;
    }

    this.setState({
      attendances,
      courses,
      currentCourse,
      currentClassroom,
      currentCourseClassrooms,
      loading: false,
    });
  }

  async handleChange(event) {
    const { courses } = this.state;
    const { value, name } = event.target;

    if (value) {
      switch (name) {
        case 'currentCourse':
          {
            this.setState({ loading: true });
            const date = moment().format('DD-MM-YYYY');
            const currentCourse = courses.filter(course => course._id === value)[0] || '';
            const currentCourseClassrooms = currentCourse
              ? await fetchClassesInCourse(currentCourse._id) : [];
            const currentClassroom = currentCourseClassrooms[0]
              ? await fetchClassById(currentCourseClassrooms['0']._id) : '';
            let attendances = null;

            if (_.get(currentClassroom, 'schedule.items.length')) {
              attendances = currentClassroom.schedule.items
                .filter(item => moment(item.start).format('DD-MM-YYYY') === date)
                .map(item => item.attendance)[0] || null;
            }

            this.setState({
              attendances,
              currentCourse,
              currentClassroom,
              currentCourseClassrooms,
              loading: false,
              date,
            });
          }
          break;
        case 'currentClassroom':
          {
            this.setState({ loading: true });
            const currentClassroom = await fetchClassById(value);
            const date = moment().format('DD-MM-YYYY');
            let attendances = null;

            if (_.get(currentClassroom, 'schedule.items.length')) {
              attendances = currentClassroom.schedule.items
                .filter(item => moment(item.start).format('DD-MM-YYYY') === date)
                .map(item => item.attendance)[0] || null;
            }

            this.setState({
              currentClassroom,
              attendances,
              loading: false,
              date,
            });
          }
          break;
        case 'date':
          {
            const { currentClassroom } = this.state;
            let attendances = null;

            if (_.get(currentClassroom, 'schedule.items.length')) {
              attendances = currentClassroom.schedule.items
                .filter(item => moment(item.start).format('DD-MM-YYYY') === value)
                .map(item => item.attendance)[0] || null;
            }

            this.setState({
              date: value,
              attendances,
            });
          }
          break;
        default:
          this.setState({ [name]: value });
      }
    }
  }

  async studentAttended(studentId) {
    const { pushQueueTopLoading } = this.props;
    const { currentClassroom, date } = this.state;
    let { attendances } = this.state;

    pushQueueTopLoading(studentId);

    if (studentId && attendances.indexOf(studentId) > -1) {
      attendances = attendances.filter(item => item !== studentId);
    } else {
      attendances.push(studentId);
    }

    currentClassroom.schedule.items = currentClassroom.schedule.items
      .map((item) => {
        const schedule = item;
        if (moment(item.start).format('DD-MM-YYYY') === date) {
          schedule.attendance = attendances;
        }
        return schedule;
      });

    const { _id, schedule, courseRefs } = currentClassroom;

    this.setState({ currentClassroom, attendances });

    await updateClass(_id, { schedule, courseRefs });
    pushQueueTopLoading(studentId);
  }

  renderCourseOption() {
    const { courses } = this.state;
    return courses.map(course => (
      <MenuItem
        key={course._id}
        style={{ textTransform: 'capitalize' }}
        value={course._id}
      >
        {course.name}
      </MenuItem>
    ));
  }

  renderClassroomOption() {
    const { currentCourseClassrooms } = this.state;
    return currentCourseClassrooms.map(classroom => (
      <MenuItem
        key={classroom._id}
        style={{ textTransform: 'capitalize' }}
        value={classroom._id}
      >
        {classroom.name}
      </MenuItem>
    ));
  }

  renderScheduleOption() {
    const { currentClassroom } = this.state;
    return currentClassroom.schedule.items.map(schedule => (schedule.start ? (
      <MenuItem
        key={moment(schedule.start).valueOf()}
        value={moment(schedule.start).format('DD-MM-YYYY')}
      >
        {moment(schedule.start).format('DD-MM-YYYY')}
      </MenuItem>
    ) : (
      ''
    )));
  }

  render() {
    const {
      courses,
      currentCourse,
      currentCourseClassrooms,
      currentClassroom,
      date,
      loading,
      attendances,
    } = this.state;
    const { classes } = this.props;

    if (
      !currentCourseClassrooms || !courses || loading
    ) {
      return (
        <div className={classes.loadingContainer}>
          <img src={loadingIcon} alt="Loading" />
        </div>
      );
    }

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={3}>
            <FormControl className={classes.formControl}>
              <Select
                style={{ textTransform: 'capitalize' }}
                value={currentCourse ? currentCourse._id : ''}
                onChange={this.handleChange}
                displayEmpty
                name="currentCourse"
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Choose Course</em>
                </MenuItem>
                {this.renderCourseOption()}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={2}>
            <FormControl className={classes.formControl}>
              <Select
                style={{ textTransform: 'capitalize' }}
                value={currentClassroom ? currentClassroom._id : ''}
                onChange={this.handleChange}
                displayEmpty
                name="currentClassroom"
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Choose Classroom</em>
                </MenuItem>
                {this.renderClassroomOption()}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={2}>
            <FormControl className={classes.formControl}>
              <Select
                style={{ textTransform: 'capitalize' }}
                value={date || ''}
                onChange={this.handleChange}
                displayEmpty
                name="date"
                className={classes.selectEmpty}
              >
                <MenuItem value="">
                  <em>Date</em>
                </MenuItem>
                { currentClassroom && currentClassroom.schedule
                  ? this.renderScheduleOption() : '' }
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            {attendances ? (
              <AttendanceTable
                studentAttended={this.studentAttended}
                attendances={attendances}
                students={currentClassroom && currentClassroom.members
                  ? currentClassroom.members.studentRefs : []}
              />
            ) : ''}
          </Grid>
        </Grid>
      </div>
    );
  }
}

Attendance.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  pushQueueTopLoading: PropTypes.func.isRequired,
};

export default withStyles(styles)(Attendance);
