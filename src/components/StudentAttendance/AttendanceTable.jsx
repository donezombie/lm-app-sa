import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {
  TableCell, TableSortLabel, Paper, Checkbox,
} from '@material-ui/core';
import {
  AutoSizer, Column, SortDirection, Table,
} from 'react-virtualized';

const styles = theme => ({
  table: {
    fontFamily: theme.typography.fontFamily,
  },
  flexContainer: {
    display: 'flex',
    alignItems: 'center',
    boxSizing: 'border-box',
  },
  tableRow: {
    cursor: 'pointer',
  },
  tableRowHover: {
    '&:hover': {
      backgroundColor: theme.palette.grey[200],
    },
  },
  tableCell: {
    flex: 1,
  },
  noClick: {
    cursor: 'initial',
  },
});

class MuiVirtualizedTable extends PureComponent {
  constructor(props) {
    super(props);

    this.getRowClassName = this.getRowClassName.bind(this);
    this.cellRenderer = this.cellRenderer.bind(this);
    this.headerRenderer = this.headerRenderer.bind(this);
  }

  getRowClassName({ index }) {
    const { classes, rowClassName } = this.props;

    return classNames(classes.tableRow, classes.flexContainer, rowClassName, {
      [classes.tableRowHover]: index !== -1,
    });
  }

  cellRenderer({ cellData, columnIndex = null }) {
    const {
      columns, classes, rowHeight, studentAttended, attendances,
    } = this.props;
    return (
      <TableCell
        component="div"
        className={classNames(classes.tableCell, classes.flexContainer)}
        variant="body"
        style={{ height: rowHeight }}
        align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
      >
        { columnIndex === 3
          ? (
            <Checkbox
              onClick={() => {
                studentAttended(cellData);
              }}
              checked={attendances.indexOf(cellData) > -1}
            />
          ) : cellData }
      </TableCell>
    );
  }

  headerRenderer({
    label, columnIndex, dataKey, sortBy, sortDirection,
  }) {
    const {
      headerHeight, columns, classes, sort,
    } = this.props;
    const direction = {
      [SortDirection.ASC]: 'asc',
      [SortDirection.DESC]: 'desc',
    };

    const inner = !columns[columnIndex].disableSort && sort != null ? (
      <TableSortLabel active={dataKey === sortBy} direction={direction[sortDirection]}>
        {label}
      </TableSortLabel>
    ) : (
      label
    );

    return (
      <TableCell
        component="div"
        className={classNames(classes.tableCell, classes.flexContainer, classes.noClick)}
        variant="head"
        style={{ height: headerHeight }}
        align={columns[columnIndex].numeric || false ? 'right' : 'left'}
      >
        {inner}
      </TableCell>
    );
  }

  render() {
    const { classes, columns, ...tableProps } = this.props;
    return (
      <AutoSizer>
        {({ height, width }) => (
          <Table
            className={classes.table}
            height={height}
            width={width}
            {...tableProps}
            rowClassName={this.getRowClassName}
          >
            {columns.map((
              {
                cellContentRenderer = null, className, dataKey, ...other
              },
              index,
            ) => {
              let renderer;
              if (cellContentRenderer != null) {
                renderer = cellRendererProps => this.cellRenderer({
                  cellData: cellContentRenderer(cellRendererProps),
                  columnIndex: index,
                });
              } else {
                renderer = this.cellRenderer;
              }

              return (
                <Column
                  key={dataKey}
                  headerRenderer={headerProps => this.headerRenderer({
                    ...headerProps,
                    columnIndex: index,
                  })
                  }
                  className={classNames(classes.flexContainer, className)}
                  cellRenderer={renderer}
                  dataKey={dataKey}
                  {...other}
                />
              );
            })}
          </Table>
        )}
      </AutoSizer>
    );
  }
}

MuiVirtualizedTable.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      cellContentRenderer: PropTypes.func,
      dataKey: PropTypes.string.isRequired,
      width: PropTypes.number.isRequired,
    }),
  ).isRequired,
  headerHeight: PropTypes.number,
  rowClassName: PropTypes.string,
  rowHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.func]),
  sort: PropTypes.func,
  studentAttended: PropTypes.func.isRequired,
  attendances: PropTypes.arrayOf(PropTypes.string).isRequired,
};

MuiVirtualizedTable.defaultProps = {
  headerHeight: 56,
  rowHeight: 56,
  rowClassName: null,
  sort: null,
};

const WrappedVirtualizedTable = withStyles(styles)(MuiVirtualizedTable);

function AttendanceTable(props) {
  const { students, studentAttended, attendances } = props;
  const rows = students.map((student) => {
    const { name, email } = student.info;
    const { _id } = student;
    const { firstName, lastName } = name;
    return {
      firstName,
      lastName,
      email,
      _id,
    };
  });

  return (
    <Paper style={{ height: rows.length * 56 + 56, width: '100%' }}>
      <WrappedVirtualizedTable
        studentAttended={studentAttended}
        attendances={attendances}
        rowCount={rows.length}
        rowGetter={({ index }) => rows[index]}
        columns={[
          {
            width: 120,
            flexGrow: 1.0,
            label: 'Last name',
            dataKey: 'lastName',
          },
          {
            width: 120,
            flexGrow: 1.0,
            label: 'First name',
            dataKey: 'firstName',
          },
          {
            width: 120,
            flexGrow: 1.5,
            label: 'E-mail',
            dataKey: 'email',
          },
          {
            width: 120,
            flexGrow: 0.5,
            label: 'Attendance',
            dataKey: '_id',
          },
        ]}
      />
    </Paper>
  );
}

AttendanceTable.propTypes = {
  students: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  studentAttended: PropTypes.func.isRequired,
  attendances: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default AttendanceTable;
