/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import queryString from 'query-string';
import PropTypes from 'prop-types';
import loadingIcon from './loading.svg';
import { axios } from '../../utils';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCheckingAuth: true,
    };
  }

  async componentDidMount() {
    const {
      token,
      removeToken,
      redirectToIdSite,
    } = this.location();
    const { isCheckingAuth } = this.state;

    removeToken();
    if (isCheckingAuth) {
      if (token && await this.login(token)) {
        this.setState({
          isCheckingAuth: false,
        });
      } else if (await this.checkAuth()) {
        this.setState({
          isCheckingAuth: false,
        });
      } else {
        redirectToIdSite();
      }
    }
  }

  location() {
    const { href, search, origin } = window.location;
    const { token } = queryString.parse(search);
    const { ID_URL } = this.props;
    const removeToken = () => window
      .history
      .replaceState(
        {}, 'No token',
        href.replace(`token=${token}`, ''),
      );
    const redirectToIdSite = () => window.location.replace(`${ID_URL}?site=${origin}`);
    return {
      href,
      search,
      origin,
      token,
      removeToken,
      redirectToIdSite,
    };
  }

  async checkAuth() {
    const { API_AUTH_URL } = this.props;
    try {
      const response = await axios.get(API_AUTH_URL);
      return response.data;
    } catch (err) {
      return false;
    }
  }

  async login(token) {
    const { API_AUTH_URL } = this.props;
    try {
      const response = await axios.post(API_AUTH_URL, { token });
      return response.data.success;
    } catch (err) {
      return false;
    }
  }

  render() {
    const { isCheckingAuth } = this.state;
    const { children } = this.props;
    if (isCheckingAuth) {
      return (
        <div
          style={{
            width: '100vw',
            height: '100vh',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <img src={loadingIcon} alt="Loading icon" />
        </div>
      );
    }
    return children;
  }
}

Login.defaultProps = {
  ID_URL: '',
  API_AUTH_URL: '',
};

Login.propTypes = {
  children: PropTypes.shape([]).isRequired,
  ID_URL: PropTypes.string,
  API_AUTH_URL: PropTypes.string,
};

export default Login;
