import axios from './axios';

export const fetchCourses = query => new Promise((resolve, reject) => {
  axios
    .get('/courses', { params: new URLSearchParams(query) })
    .then(({ data }) => {
      resolve(data || []);
    })
    .catch((error) => {
      reject(error);
    });
});

export const fetchCourseById = id => new Promise((resolve, reject) => {
  axios
    .get(`/courses/${id}`)
    .then(({ data }) => {
      resolve(data || '');
    })
    .catch((error) => {
      reject(error);
    });
});
