import Axios from 'axios';
import { API_TEACHER_URL } from '../urls';

export default Axios.create({
  baseURL: API_TEACHER_URL,
  validateStatus: () => true,
  withCredentials: true,
  headers: {},
});
