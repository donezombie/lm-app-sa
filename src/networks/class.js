import axios from './axios';

export const fetchClasses = query => new Promise((resolve, reject) => {
  axios
    .get('/classes', { params: new URLSearchParams(query) })
    .then(({ data }) => {
      resolve(data || []);
    })
    .catch((error) => {
      reject(error);
    });
});

export const fetchClassById = id => new Promise((resolve, reject) => {
  axios
    .get(`/classes/${id}`)
    .then(({ data }) => {
      resolve(data || '');
    })
    .catch((error) => {
      reject(error);
    });
});

export const fetchClassesInCourse = courseId => new Promise((resolve, reject) => {
  axios
    .get(`/classes?q=${courseId}`)
    .then(({ data }) => {
      resolve(data || []);
    })
    .catch((error) => {
      reject(error);
    });
});

export const updateClass = (classId, body) => new Promise((resolve, reject) => {
  axios
    .put(`/classes/${classId}`, body)
    .then(({ data }) => {
      resolve(data || {});
    })
    .catch((error) => {
      reject(error);
    });
});
