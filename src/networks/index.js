export {
  fetchClasses, fetchClassById, fetchClassesInCourse, updateClass,
} from './class';
export { fetchCourses, fetchCourseById } from './course';
export { checkAuth, login } from './auth';
export { default as axios } from './axios';
