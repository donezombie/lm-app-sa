import axios from 'axios';
import { API_AUTH_URL } from '../urls';

axios.defaults.withCredentials = true;
axios.defaults.validateStatus = () => true;
axios.interceptors.response.use(response => response, error => Promise.reject(error));

export const checkAuth = () => new Promise((resolve, reject) => {
  axios
    .get(API_AUTH_URL)
    .then(({ data }) => {
      resolve(data);
    })
    .catch((error) => {
      reject(error);
    });
});

export const login = token => new Promise((resolve, reject) => {
  axios
    .post(API_AUTH_URL, { token })
    .then(({ data }) => {
      resolve(data);
    })
    .catch((error) => {
      reject(error);
    });
});

export const fetchClassesInCourse = courseId => new Promise((resolve, reject) => {
  axios
    .get(`/classes?q=${courseId}`)
    .then(({ data }) => {
      resolve(data || []);
    })
    .catch((error) => {
      reject(error);
    });
});

export const updateClass = (classId, body) => new Promise((resolve, reject) => {
  axios
    .put(`/classes/${classId}`, body)
    .then(({ data }) => {
      resolve(data || {});
    })
    .catch((error) => {
      reject(error);
    });
});
